#!/bin/bash

set -e

DEST="/usr/local/huni-nodejs"
SRC="/build"

cd /tmp/
curl -s -L http://nodejs.org/dist/v$VERSION/node-v$VERSION.tar.gz | tar xzf -

cd "node-v$VERSION"
./configure --prefix=$DEST
make -j$(( 1 + 2 * $( nproc ) )) install
cat "$SRC"/packages | xargs "$DEST/bin/npm" install -g

cd "$SRC"/package
debuild --no-lintian -us -uc -i -I -d -b
debuild -d clean

# USER_ID & GROUP_ID get passed in as docker env vars
chown $USER_ID:$GROUP_ID "$SRC"/huni-nodejs_*
