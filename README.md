Welcome to huni-nodejs

Node.js gets installed into /usr/local/huni-nodejs. Globally installed
Node modules are a in /usr/local/huni-nodejs/lib/node_modules.

To use it you probably want to add the following to your PATH
  /usr/local/huni-nodejs/bin


If you need to re-build this package:

Use aptitude to install all the build requirements from debian/control.
(you probably already have them if you have strategic-perl installed)

Run this to build and install Node.js (you'll need sudo access..):
bin/install_nodejs.sh

Build the debian package with
./build_deb.sh


