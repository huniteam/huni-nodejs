#!/bin/bash

docker run \
    -v $PWD/build:/build \
    -e VERSION=0.10.32 \
    -e USER_ID=$( id -u ) \
    -e GROUP_ID=$( id -g ) \
    huniteam/deb-builder:trusty
